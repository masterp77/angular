import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Curso } from '../models/curso.model';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista-cursos.component.html',
  styleUrls: ['./lista-cursos.component.css']
})
export class ListaCursosComponent implements OnInit {

  @Output() materiasEmit = new EventEmitter<string[]>();
  @Input() tutor: string;
  cursos: Curso[];
  materias: string[] = ['Espanol','Matematicas','Historia','Programacion','Idiomas'];
  constructor() { 
    this.cursos = [];
  }

  ngOnInit(): void {
    this.materiasEmit.emit(this.materias);
  }

  guardar(nombre: string, tutor:string, url:string):boolean{
    this.cursos.push(new Curso(nombre, tutor, url));
    return false;
  }
}
