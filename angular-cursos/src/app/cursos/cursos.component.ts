import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Curso } from '../models/curso.model';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  @Input() curso: Curso;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { 

   }

  ngOnInit(): void {
  }

}
