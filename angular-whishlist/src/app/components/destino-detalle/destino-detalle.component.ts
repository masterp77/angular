import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosAPiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';

 class DestinosAPiClientViejo {
  getById(id: string): DestinoViaje {
    console.log('Clase vieja');
    return null
  }
} 

interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinoApiClientDecorated extends DestinosAPiClient {
  constructor(@Inject(APP_CONFIG) config: AppConfig, store: Store<AppState>, http: HttpClient) {
    super(store, config, http);
  }
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase decorada');
    //console.log('config: ' + this.config.apiEndPoint);
    return super.getById(id);
    
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosAPiClient,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }, 
    { provide: DestinosAPiClient, useClass: DestinoApiClientDecorated }, 
    { provide: DestinosAPiClientViejo, useExisting: DestinosAPiClient}
  ],
  styles: [`
  mgl-map {
    height: 75vh;
    width: 75vw;
  }
`]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosAPiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
