import { DestinosViajesState, initialDestinosViajesState, InitMyDataAction, reducerDestinosViajes, NuevoDestinoAction } from "./destinos-viajes-state.model"
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', ()=> {
    it('Should reduce init data', () => {
        //setup donde se arma los objetos a testear
        const prevState: DestinosViajesState = initialDestinosViajesState();
        //action
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        //assert
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('Should reduce new item added', () => {
        const prevState: DestinosViajesState = initialDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url', 0, true));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});