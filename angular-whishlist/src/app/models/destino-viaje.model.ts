import {v4 as uuid} from 'uuid';

export class DestinoViaje{
    id = uuid();
//    selected: boolean;
    
    constructor(public nombre:string, public url:string, public votes: number = 0, public selected: boolean, public servicios: Servicio[] = [new Servicio('pileta', 0), new Servicio('desayuno', 0)]){      }

    isSelected():boolean{
       return this.selected;
    }

    setSelected(s:boolean){
        this.selected = s;
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}

export class Servicio{
    id = uuid();
    constructor(public nombre:string, public voto: number = 0){ 
     }

     voteUp(){
        this.voto++;
    }

    voteDown(){
        this.voto--;
    }
}